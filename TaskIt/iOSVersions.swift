//
//  iOSVersions.swift
//  TaskIt
//
//  Created by Elton Nix on 6/24/15.
//  Copyright (c) 2015 Elton Nix. All rights reserved.
//

import Foundation

let iOS8 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_7_1)
let iOS7 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_6_1)
let iOS6 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_5_1)
let iOS5 = floor(NSFoundationVersionNumber) > floor(NSFoundationVersionNumber_iOS_4_3)
let iOS4 = floor(NSFoundationVersionNumber) >= floor(NSFoundationVersionNumber_iOS_4_0)