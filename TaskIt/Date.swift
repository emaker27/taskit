//
//  Date.swift
//  TaskIt
//
//  Created by Elton Nix on 6/23/15.
//  Copyright (c) 2015 Elton Nix. All rights reserved.
//

import Foundation

class Date {
    class func from (#year: Int, month: Int, day: Int) -> NSDate {
        var gregorianCalendar: NSCalendar
        var components = NSDateComponents()
        components.year = year
        components.month = month
        components.day = day
        
        if iOS8 {
            gregorianCalendar = NSCalendar(identifier: NSCalendarIdentifierGregorian)!
        } else {
            gregorianCalendar = NSCalendar(identifier: NSGregorianCalendar)! // Deprecated in iOS 8
        }

        var date = gregorianCalendar.dateFromComponents(components)
        
        return date!
    }
    
    class func toString(#date: NSDate) -> String {
        
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "yyyy-MM-dd"
        let dateString = dateStringFormatter.stringFromDate(date)
        
        return dateString
    }
}