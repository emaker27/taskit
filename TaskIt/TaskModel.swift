//
//  TaskModel.swift
//  TaskIt
//
//  Created by Elton Nix on 9/5/15.
//  Copyright (c) 2015 Elton Nix. All rights reserved.
//

import Foundation
import CoreData

@objc(TaskModel)
class TaskModel: NSManagedObject {

    @NSManaged var completed: NSNumber
    @NSManaged var subtask: String
    @NSManaged var task: String
    @NSManaged var uuid: String

}
