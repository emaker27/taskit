//
//  TaskDetailViewController.swift
//  TaskIt
//
//  Created by Elton Nix on 6/21/15.
//  Copyright (c) 2015 Elton Nix. All rights reserved.
//

import UIKit
import CoreData

@objc protocol TaskDetailViewControllerDelegate {
    optional func taskDetailEdited()
}

class TaskDetailViewController: UIViewController, UITextFieldDelegate {

    var detailTaskModel: TaskModel!
    
    @IBOutlet weak var taskTextField: UITextField!
    @IBOutlet weak var subtaskTextField: UITextField!
    @IBOutlet weak var deleteTaskButton: UIButton!
    
    var delegate: TaskDetailViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Background")!)
        
        self.taskTextField.text = detailTaskModel.task
        self.subtaskTextField.text = detailTaskModel.subtask
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        taskTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func taskTextFieldActive(sender: UITextField) {
        taskTextField.delegate = self
    }
    
    @IBAction func subtaskTextFieldActive(sender: UITextField) {
        subtaskTextField.delegate = self
    }
    
    
    // dismiss view segue
    @IBAction func cancelButtonTapped(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func deleteTaskButtonPressed(sender: UIButton) {
        var alert = UIAlertController(title: "WARNING", message: "Are you sure you wish to delete this item?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.Destructive, handler: { (alert: UIAlertAction!) -> Void in
            self.deleteItemFromCoreData()
            self.navigateBack()
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func doneBarButtonItemPressed(sender: UIBarButtonItem) {
        editTask()
    }
    
    func editTask() {
        taskTextField.resignFirstResponder()
        
        detailTaskModel.task = taskTextField.text
        detailTaskModel.subtask = subtaskTextField.text
        detailTaskModel.completed = detailTaskModel.completed
        
        ModelManager.instance.saveContext()
        
        navigateBack()
    }
    
    func navigateBack() {
        self.navigationController?.popViewControllerAnimated(true)
        delegate?.taskDetailEdited!()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        editTask()
        return true
    }
    
    func deleteItemFromCoreData() {
        let taskUuid = detailTaskModel.uuid
        let fetchRequest = NSFetchRequest(entityName:"TaskModel")
        let predicate = NSPredicate(format: "uuid == %@", taskUuid)
        fetchRequest.predicate = predicate
        
        let objectContext = ModelManager.instance.managedObjectContext!
        let fetchedEntities = objectContext.executeFetchRequest(fetchRequest, error: nil) as! [TaskModel]
        let entityToDelete = fetchedEntities.first
        
        if fetchedEntities.count == 1 {
            objectContext.deleteObject(entityToDelete!)
            objectContext.save(nil)
            println("Object deleted.")
        } else {
            println("ERROR: The object wasn't deleted. Count of fetchedEntities = \(fetchedEntities.count)")
        }
    }
}
