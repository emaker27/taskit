//
//  AddTaskViewController.swift
//  TaskIt
//
//  Created by Elton Nix on 6/24/15.
//  Copyright (c) 2015 Elton Nix. All rights reserved.
//

import UIKit
import CoreData

protocol AddTaskViewControllerDelegate {
    func addTask(message: String)
    func addTaskCanceled(message: String)
}

class AddTaskViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var taskTextField: UITextField!
    @IBOutlet weak var subtaskTextField: UITextField!
    
    var delegate: AddTaskViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Background")!)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        taskTextField.delegate = self
        taskTextField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Dismiss view modal
    @IBAction func cancelButtonTapped(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
        delegate?.addTaskCanceled("Task was not added!")
    }
    
    @IBAction func taskTextFieldActive(sender: UITextField) {
        sender.delegate = self
    }
    
    @IBAction func subtaskTextFieldActive(sender: UITextField) {
        sender.delegate = self
    }
    
    @IBAction func addTaskButtonTapped(sender: UIButton) {
        addTask()
    }
    
    func addTask() {
        taskTextField.resignFirstResponder()
        
        let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let managedObjectContext = ModelManager.instance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("TaskModel", inManagedObjectContext: managedObjectContext!)
        
        if taskTextField.text != "" {
            let task = TaskModel(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext!)
            
            if NSUserDefaults.standardUserDefaults().boolForKey(kShouldCapitalizeTaskKey) == true {
                task.task = taskTextField.text.capitalizedString
            } else {
                task.task = taskTextField.text
            }
            
            task.subtask = subtaskTextField.text
            
            if NSUserDefaults.standardUserDefaults().boolForKey(kShouldCompleteNewTodoKey) == true {
                task.completed = true
            } else {
                task.completed = false
            }
            
            task.uuid = NSUUID().UUIDString
            
            ModelManager.instance.saveContext()
            
            var request = NSFetchRequest(entityName: "TaskModel")
            var error: NSError? = nil
            
            var results: NSArray = managedObjectContext!.executeFetchRequest(request, error: &error)!
            
            for res in results {
                println(res)
            }
            
            self.dismissViewControllerAnimated(true, completion: nil)
            delegate?.addTask("Task Added")
        } else {
            alertWithText(header: "Missing Info", message: "Please enter at least the Item name")
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        addTask()
        return true
    }
    
    func alertWithText(header: String = "Warning", message: String, buttonText: String = "Ok") {
        var alert = UIAlertController(title: header, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: buttonText, style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
}
